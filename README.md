# Claat system

Tool for generating static pages

The pages generated for this repo is on [gitlab.io](https://19s_itt4_internship.gitlab.io/claat-template-system/)

# Goal
The main goal is to create a static generator for exercises for next semester in ITT

## Description

The program takes an input in form of a resource location, which can either be a Google Doc ID, markdown, local file path or an arbitrary URL. 
It then converts the input into a codelab format, HTML by default.


## How to use

There is a simple guide how to instal and run **claat** inside the [test](https://gitlab.com/19s_itt4_internship/claat-template-system/tree/master/tests/1) folder. 
Simply clone this repo and navigate to **test/1** folder and open **index.html** with your favorite browser.



