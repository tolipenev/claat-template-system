author:            Anatoli Penev
summary:           Test page for claat
id:                claat-guide
categories:        static page
environments:      markdown, go
status:            final
feedback link:     <https://19s_itt4_internship.gitlab.io/claat-template-system/>
analytics account: 0

# Installing claat on local machine and running a test example

## Overview of the tutorial

Duration: 0:05

This tutorial shows you how to set up claat tools and make an example page. In this tutorial you will do the following: 

* Learn about claat tools and install them for local use.
* Follow a template on how to create your own static page.

Prerequesites

* Windows or Linux installed
* Basic knowledge of bash/cmd

## Install

Duration: xxx

The easiest way is to download pre-compiled binary. The binaries, as well as their checksums are available at the
[Releases pagrelease's page](https://github.com/googlecodelabs/tools/releases/latest).

Alternatively, if you have [Go installed](https://golang.org/doc/install):

    `go get github.com/googlecodelabs/tools/claat`

If none of the above works, compile the tool from source following Dev workflow
instructions below.

![release's page](images/claat-guide/release_page.png)

## Dev workflow

Prerequisites

1. Install [Go](https://golang.org/dl/) if you don't have it.
2. Make sure this directory is placed under             *$GOPATH/src/github.com/googlecodelabs/tools*.
3. Install package dependencies with go get *./...* from this directory.

To build the binary, *run make.*

Testing is done with *make test* or *go test ./...* if preferred.

Don't forget to run *make lint* or *golint ./...* before creating a new CL.

To create cross-compiled versions for all supported OS/Arch, run *make release*. It will place the output in *bin/claat-<os>-<arch>*.

## Building from source

Duration: xxx

Download a bin file (for me it is *claat-linux-amd64*) and save on disk and give executive permissions *chmod +x claat-linux-amd64*. It is worth to add this directory to PATH to have this tool available from any place. In my case in *~/.bashrc* I have added that line:

**export PATH=$PATH:/home/username/bin/**

If all works when we run *claat-linux-amd64 help* we should get help message.
